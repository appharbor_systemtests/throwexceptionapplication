﻿using System;
using System.Configuration;
using System.Threading;

namespace ThrowExceptionApplication
{
	public class Program
	{
		public static void Main(string[] args)
		{
			Thread.Sleep(5000);
			throw new Exception(ConfigurationManager.AppSettings["ExceptionMessage"]);
		}
	}
}
